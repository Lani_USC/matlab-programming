% Basic if intro

clear
clc

N = 9;

if N<10
    fprintf('Whoopee! I am inside the loop.\n')
    fprintf('%g is less than 10.\n\n',N)
end

fprintf('End of demo.\n\n')