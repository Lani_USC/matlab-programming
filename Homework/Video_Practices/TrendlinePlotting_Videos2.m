%  polyfit demo video: Power Models

% Housekeeping
clear
close all
clc

% Set up data and plot
C = [1.3 2.7 3.5 4.1 4.8];
E = [10 37 67 97 137];
figure('Color','w')
plot(C,E,'r*','LineWidth',2)
grid on
ylabel('Energy (E) [J]','FontWeight','Bold')
xlabel('Compression (C) [cm]','FontWeight','Bold')
title('Energy stored in a Spring','FontWeight','Bold')
axis([0 5 0 150])
set(gca,'ytick',0:25:150,'FontWeight','Bold')

pause

% Calculate linear model parameter
MC = polyfit(log10(C),log10(E),1);
m = MC(1);
b = 10^MC(2);

% Set up theoretical data set
TC = 1:.1:5;
TE = b*TC.^m;

% Add theoretical line to graph
hold on
plot(TC,TE,'r:','LineWidth',2.5)

pause

% Add equation for model to the graph
PE = sprintf('E = %.2f C^{%.0f}',b,m);
text(1,100,PE,'Color','r','BackgroundColor','w','EdgeColor','r')
