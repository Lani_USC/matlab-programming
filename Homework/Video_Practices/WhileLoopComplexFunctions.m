%{
While loops - estimating complex mathematical functions
determine x given e^x * tan(x)=C, where C is positive.
NOTE: This function is monotonic increasing between 0 and pi/2
%}

% NOTE: MIGHT solve mathematically using Euler's Identity
%  HOWEVER... Display graph, explain basic algorithm graphically

% Housekeeping
clear
clc
close all

% Get C and desired accuracy
C = input('Enter a value greater than 0: '); % Might error check
PercErr = input('Enter a desired maximum error in percent: ');

Low = 0; % Initial Lower guess bound
High = pi/2; % Intial Upper hues bound
Guess = (High+Low)/2; % Initial Guess (pi/4 ? 0.785)
PercErr = PercErr/100; % Convert to fraction
GuessCount = 1; % Keep track of guesses

FVal = exp(Guess)*tan(Guess);
ERR = (C-FVal)/C; % Calculate error
% NOTE: both e^x  and tan(x) increase as x increases, thus if the
% calculated function value is too high, the guess is too high.
% Since we are subtracting calculated value from target value,
% error is negative if guess is too high.
while abs(ERR)>PercErr
    GuessCount = GuessCount+1;
    if ERR<0   % Error negative means guess too high
        High = Guess;  % Update upper limit
        Guess = (High+Low)/2;  % Update Guess
    else
        Low = Guess;
        Guess = (High+Low)/2;
    end
    FVal = exp(Guess)*tan(Guess);
    ERR = (C-FVal)/C; % Calculate error
end

fprintf('x = %.8f giving a funvtion value of %.10f after %g interations.\n',Guess,exp(Guess)*tan(Guess),GuessCount)
