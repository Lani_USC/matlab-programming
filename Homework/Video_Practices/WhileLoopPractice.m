% Infinite loop

clear
clc

Count = 0;
while 1<2
    fprintf('%12g\n',Count)
    Count = Count+1;
    pause(1)
end
