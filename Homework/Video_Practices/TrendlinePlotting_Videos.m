% polyfit demo video: Linear Models

% Housekeeping
clear
close all
clc

% Set up data and plot
I = [5.3 9.2 17.1 19.6 28];
P = [630 1140 1990 2170 3510];
figure('color','w')
plot(I,P,'r*','MarkerSize',14,'LineWidth',2)
grid on
ylabel('Power (P) [W]')
xlabel('Current (I) [A]')
title('Power Delivered to a Widget')
axis([0 30 0 4000])
set(gca,'ytick',0:1000:4000)

pause

% Calculate linear model parameters
MC = polyfit(I,P,1);
m = MC(1)
b = MC(2)

pause

% Set up theoretical data set
TI = 5:28;
TP = m*TI+b;

% Add theoretical line to graph
hold on
plot(TI,TP,'r:','LineWidth',2.5)

pause

% Add equation for model to graph
PE = sprintf('P = %.0f I + %.1f',m,b);
text(10,3000,PE,'Color','r','FontSize',12,'BackgroundColor','w','EdgeColor','r')
