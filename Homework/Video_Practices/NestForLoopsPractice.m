% Basic Nested for  loops

clear
clc

for I1=1:3:10
    fprintf('I1=%g\n',I1)
    for I2=1:3
        fprintf('\tI2=%g\n',I2)
    end
    pause
end

