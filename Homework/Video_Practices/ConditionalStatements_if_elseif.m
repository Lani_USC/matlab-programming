% if - elseif

clear
clc

% All temperatures in degrees Celcius
MeltingPt = 0;
BoilingPt = 100;
AbsZero = -273.15;

Temp = input('Enter the temperature of the water: ');

if Temp>BoilingPt
    fprintf('The water is gaseous.\n')
elseif Temp>MeltingPt
    fprintf('The water is liquid.\n')
elseif Temp>AbsZero
    fprintf('The water is solid.\n')
else
    beep
    fprintf('%.1f is not valid. It is below absolute zero.\n',Temp)
end

