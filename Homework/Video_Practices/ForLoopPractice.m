% Simple for loops

clear
clc

fprintf('A\tsin(A)\tcos(A)\ttan(A)\n')

for A=0:pi/6:pi
    fprintf('%.3f\t%.3f\t%.3f\t%.3f\n',A,sin(A),cos(A),tan(A))
    pause
end
