%  polyfit demo video: Exponetial Models

%{
Decay of brightness of phosphorescent strontium aluminate relative to
initial brightness. NOTE: must be doped with other elements such as
europium or dysprosium

Intitial Brightness arbitrarily considered 1
%}

clear
close all
clc

% Define data
t = [0 0.3 0.75 1.1 1.7 2.9 4.2]; % Measurement times [Hr]
LR = [1 0.78 0.62 0.45 0.28 0.13 0.068]; % Relative Brightness L/L0

% Plot experimental data
figure('color','w')
plot(t,LR,'rh','MarkerSize',12,'MarkerFaceColor','r')
pause

% Graph Details
grid on
AH = gca;
AH.FontSize = 18; % NOTE: Include axis labels
pause

xlabel('Time (t) [hr]','FontSize',16)
pause

YL = ylabel('Relative Brightness L/L_0'); % Using text object (since 2014b)
pause
YL.FontSize = 16;
pause

T = title({'Phosphorescent Decay';'SrAl Compound 17B'}); % To get multiple lines
T.FontSize = 16; % NOTE: Title is bold by default
pause

% Calculate expontential model parameters
ExpParams = polyfit(t,log(LR),1);
m = ExpParams(1);
b = exp(ExpParams(2));

% Create theoretical data set
t_theo = linspace(t(1),t(length(t)));
LR_theo = b*exp(m*t_theo);

% Plot trendline
hold on
plot(t_theo,LR_theo,'r-','LineWidth',2)
pause

% Add trendline equation to graph
TLEq = sprintf('L/L_0 = %.3f e^{%.3f t}',b,m);

TX = text(2.15,0.4,TLEq);
pause
TX.FontSize = 16;
TX.Color = 'r';
TX.BackgroundColor = 'w';
TX.EdgeColor = 'r';

