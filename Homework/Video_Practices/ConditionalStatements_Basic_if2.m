% Basic if statement

clear
clc

Target = randi(100,1);
Prompt = sprintf('Enter a positive value that has a square root of %g: ',Target);
Guess = input(Prompt);
if Guess<Target^2
    beep
    fprintf('\nYour guess is too small.\n')
    fprintf('The square root of %.0f is %.3f, not %g.\n\n',Guess,sqrt(Guess),Target)
elseif Guess>Target^2
    beep
    fprintf('\nYour guess is too high.\n')
    fprintf('The square root of %.0f is %.3f, not %g.\n\n',Guess,sqrt(Guess),Target)
else
    fprintf('Correct!\n')
    fprintf('The square root of %.0f is indeed %g.\n\n',Guess,Target)
end

