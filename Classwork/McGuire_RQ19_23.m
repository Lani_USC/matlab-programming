%{
Lani McGuire           MatLab Programming (ENGIN141)         April 30, 2019


Program Description: This code prompts the user for a 4x4 matrix and then
determines whether or not that matrix is magic or not and returns the
features of the matrix, error checking for positive numbers and ensuring 
that it is a 4x4 matrix. Review Question 19-23.


VARIABLES

    INPUTS
        flag1         trigger for while loop 1
        usermatrix    matrix inputted by the user
        rows          number of rows in usermatrix
        columns       number of columns in usermatrix
        flag2         trigger for while loop 2
        x             control for for loop 1
        logic_c       logic statement determining if the matrix is 4x4 or
                      not
        colsums       sum of each column of usermatrix
        rowsums       sum of each row of usermatrix
        eqtest        vector of the column and row sums
        mc            the magic constant (only if the matrix is a magic
                      square
        y             control for for loop 2
        nxtchance     user chooses whether to try another matrix if theirs
                      isn't a magic square
        mag           contains yes or no, pertaining to if the square is
                      semi-magic or not
        diag          the numbers from the diagonal
        z             the control for for loop 3
        diagsums      the sum of the diagonal
        logic_e       logic statement determining if the diagonal sum is
                      equal to the magic constant or not
        norm          contains yes or no, pertaining to if the magic square
                      is normal or not
        perf          contains yes or no, pertaining to if the magic square
                      is perfect or not
    OUTPUTS
        N/A

NOTE: I was wrong earlier about the for loops; each one of the ones where
is said a for loop was required, a for loop was required. I discovered so
after a series of tests as well as trial and error with my code, but I
eventually figured it out.
%}

%% Setup

close all
clear
clc

%% Part A: Prompt user to input a 4x4 matrix

flag1 = 0;    % trigger for the big while loop

while flag1==0    % while loop 1
% this while loop (while loop 1) first checks for nonpositive numbers in 
% the user inputted matrix, makes sure it is 4x4, and determines if the 
% matrix entered is a magic square; if it isn't a magic square, the user 
% has the opportunity to start over and try again
    
    clc
    
    % allows the user to input a matrix and determines how many rows and
    % columns the matrix has
    usermatrix = input('Please input a proposed magic square:\n');
    [rows,columns] = size(usermatrix);
    
%% Part B: Check for negative numbers or zero
    
    flag2 = 0;     % controls this upcoming, smaller while loop

    while flag2==0    % while loop 2  
        
        for x = 1:rows*columns    % for loop 1
        % will check each matrix value 1-by-1 until it finds a negative
        % number
        
            if usermatrix(x)<=0
            % when a negative number is found, the user is promted to enter
            % another matrix; then they return to the top of this inner 
            % while loop (while loop 2); will continue until they enter a 
            % matrix without negative numbers or zeros
                flag2 = 0;    % ensures that while loop 2 doesn't end
                warning('Matrix cannot contain any negative numbers or zeros')
                fprintf('\nTry again\n')
                usermatrix = input('Please input a matrix without negative numbers or zeros:\n');
                [rows,columns] = size(usermatrix);
                break    % force stops for loop 1
            else
            % if no negative number is found, the inner while loop ends and
            % the code continues
                flag2 = 1;    % ends the inner while loop
            end    % end of if statement
            
        end    % end of for loop 1
        
    end    % end of while loop 2

%% Part C: Make sure a 4x4 matrix is inputted
    
    % determines if there are 4 rows and columns
    logic_c = rows==4&&columns==4;

    while logic_c==0    % while loop 3
    % if they aren't, the user is prompted to input another matrix until 
    % they enter 4x4 matrix; like the instructions advise in the book, it 
    % doesn't check negative for numbers this time around
    
        warning('Matrix is not 4x4')
        fprintf('\nTry again\n')
        usermatrix = input('Please input a 4x4 matrix:\n');
        [rows,columns] = size(usermatrix);
        logic_c = rows==4&&columns==4;
        
    end    % end of while loop 3

%% Part D: Check to determine if the matrix is a magic square or not

%{
NOTE: I merged the solution to Part E.1 with this section because it would
have been repetative otherwise.
%}

    % finds the sums of the rows and columns
    colsums = sum(usermatrix,1);    % columns
    rowsums = sum(usermatrix,2)';   % rows
    eqtest = [colsums rowsums];     % combined   
    
    % if this is a magic square, all the sums should be equal so it doesn't
    % matter which one I use to define the magic constant
    mc = colsums(1);
    
    for y=1:8    % for loop 2
    % will check each of the sums until it finds one that is not equal to
    % the magic constant
    
        if eqtest(y)~=mc
        % if the matrix is not a magic square, the user is given the
        % opportunity to start over from the top
            nxtchance = menu('You have not entered a magic square. Would you like to try again?','Yes','No');
            if nxtchance==2
            % if they choose not to, while loop 1 ends
                mag = 'No ';
                flag1 = 1;    % ends while loop 1
                break    % force stops for loop 2
            elseif nxtchance==0
            % if they x out the menu, code ends
                error('You didn''t choose from the menu')
            else
            % if they choose to try again, code goes to the beginning of
            % while loop 1
                flag1 = 0;    % ensures that while loop 1 does not end
                break    % force stops for loop 2
            end    % end of if statement
        else
        % If the matrix is a magic square, it is noted to be semi-magic and
        % while loop 1 ends, allowing code to continue
            flag1 = 1;    % ends while loop 1
            mag = 'Yes';
        end    % end of if statement
        
    end    % end of for loop 2
    
end    % end of while loop 1
    
%% Part E: Classification of magic square

% prelocation of vector containing the diagonal
diag = zeros(1,4);

for z = 1:4    % for loop 3
% draws the numbers on the diagonal

    diag(z) = usermatrix(z,z);
    
end    % end of for loop 3

% compares the sum of the numbers on the diagonal to the magic constant
diagsums = sum(diag);
logic_e = diagsums==mc;

if logic_e==1
% if it is the same, then the magic square is normal
    norm = 'Yes';
else
% otherwise, it is not normal
    norm = 'No ';
end    % end of if statement

if max(usermatrix,[],'all')==16&&strcmp(mag,'Yes')&&strcmp(norm,'Yes')
% if the matrix is magic and its greatest number is 16, then it is perfect
    perf = 'Yes';
else
% if either one is false, it is not perfect
    perf = 'No ';
end    % end of if statement
    
%% Output the classification of the matrix

clc

fprintf('This is your matrix:\n\n')
disp(usermatrix)

if strcmp(mag,'Yes')
% if the matrix is a magic square, the information for the classification
% of the magic square is displayed
    fprintf('You inputted a magic square!\n')
    fprintf('The magic constant for your magic square is %.0f.\n',mc)
    fprintf('The classification for your magic square is:\n')
    fprintf('\nSemi-magic\t\tNormal\t\tPerfect\n')
    fprintf('%s\t\t\t\t%s\t\t\t%s\n\n',mag,norm,perf)
else
% if it is not a magic square, oh well
    fprintf('You did not input a magic square.\n')
    fprintf('The classification for your square is:\n')
    fprintf('\nSemi-magic\t\tNormal\t\tPerfect\n')
    fprintf('%s\t\t\t\t%s\t\t\t%s\n\n',mag,norm,perf)
end    % end of if statement 
