%% Lani McGuire          MatLab Programming (ENGIN141)        March 5, 2019

% Program Description: A proper plot of the theoretical voltage decay of a
% resistor-capacitor circuit.

%% Variables

%   INPUTS
%       C - capacitance of the circuit in microfarads
%       R - resitance of the curcuit in ohms
%       V1 - initial voltage in volts
%       t - vector containing time in microseconds
%       V - vector containing voltage in volts
%   OUTPUTS
%       N/A

%% Setup

close all
clear
clc

%% Inputs

C = 500;        % units -> microfarads
R = 0.5;        % units -> ohms
V1 = 10;        % units -> volts
t = 0:1:600; % range from 1 to 600; units -> microseconds

% calculations
V = V1.*exp((-1.*t)./(R*C)); % calculates voltage; units -> volts

%% Outputs

% creates chart
figure('color','w')
plot(t,V)
title('Voltage Decay of a Resistor-Capacitor Circuit over Time')
xlabel('Time (t) [\mus]')
ylabel('Voltage (V) [V]')
grid on
