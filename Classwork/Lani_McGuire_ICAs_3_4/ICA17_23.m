%% Lani McGuire         MatLab Programming ()          March 4, 2019

% Program Description: Create two plot that shows the theoretical
% relationship between orbital period and orbital radius. One uses a linear
% axis, while the other uses a logarithmic axis.

%% Variables

%   INPUTS
%       P1 - orbital period of Earth [yr]
%       r1 - distance from the sun in [au]
%       C - constant for the ratio of [(P^2)/(r^3)]
%       r - vector containing the distances from the sun
%       P - vector containing orbital period corresponding to distances
%   OUTPUTS
%       N/A

%% Setup
close all
clear
clc

%% Inputs

P1 = 1;              % units -> year [yr]
r1 = 1;              % units -> astronomical unit [au]
C = (P1^2)/(r1^3);   % calculates the constant
r = 0.2:50;          % distances from the sun from .2 to 50; units -> [au]
P = sqrt(C.*(r.^3)); % orbital periods from radius .2-50; units -> [yr]

%% Outputs

% opens figure window
figure('color','w')

% creates first plot on top (regular axes)
subplot(2,1,1)
plot(r,P)
title('Relationship Between Distance from Sun and Orbital Period: Linear Axes')
xlabel('Distance from Sun (r) [au]')
ylabel('Orbital Period (P) [yr]')
grid on

% creates second plot on bottom (logarithmic axes)
subplot(2,1,2)
loglog(r,P)
title('Relationship Between Distance from Sun and Orbital Period: Logarithmic Axes')
xlabel('Distance from Sun (r) [au]')
ylabel('Orbital Period (P) [yr]')
grid on

