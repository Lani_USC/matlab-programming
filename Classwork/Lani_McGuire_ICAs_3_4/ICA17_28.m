%% Lani McGuire          MatLab Programming (ENGIN141)        March 5, 2019

% Program Description: Graph cos(theta) between the angles 0 and 720, in
% units degrees and graph a cosine graph from user chosen values. Equation 
% shown below.

%% Variables

%   INPUTS
%       theta   vector containing angles from 0 to 720 degrees
%       norm    normal sine equation
%       A       user inputed value
%       B       user inputed value
%       C       user inputed value
%       D       user inputed value
%       y       chosen equation to graph
%       txt1    equation 1
%       txt2    equation 2
%   OUTPUTS
%       N/A

%% Setup

close all
clear
clc

%% Inputs

theta = 0:720;                                                            % theta values in degrees
norm = cosd(theta);
fprintf('\n\nEquation:\ny = Acos(B\theta + C) + D\n\n')                   % displays equation
A = input('\nInput a number corresponding to A in the above equation\n'); % defines A
B = input('\nInput a number corresponding to B in the above equation\n'); % defines B
C = input('\nInput a number corresponding to C in the above equation\n'); % defines C
D = input('\nInput a number corresponding to D in the above equation\n'); % defines D
y = A.*cosd(B.*theta+C)+D;                                                % creates the equation
txt1 = sprintf('y = cos(\theta)');
txt2 = sprintf('y = %.0fcos(%.0f\theta + %.0f) + %.0f',A,B,C,D);

%% Outputs

% creates plot
figure('color','w')
title('Graph of cos(\theta)')

% plot norm first, then chosen
plot(theta,norm,'b-',theta,y,'r--')


xlabel('Angle (\theta)')
ylabel('Function (unitless)')
legend(txt1,txt2)
lgd = legend;
lgd.Location = 'Best';
grid on
