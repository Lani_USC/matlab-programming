%% Lani McGuire           MatLab Programming ()           February 19, 2019

% Program Description: (ICA 17-18) Program will tabulate information about
% various spacecraft eploring other planets and moons in our solar system.

%% Variables
%   INPUTS
%       PName      a 1x17 cell array containing the names of many moons and
%                  planets
%       PData      a 3x17 matrix containing the radii, masses, and
%                  gravitational acceleration of the corresponding bodies 
%                  in PName
%       Vol        volume of the planet in [m^3]
%       Dens       density of the planet in [kg/m^3]
%       Min_d      holds the number of the space of the least dense body
%       Craft      name of the spacecraft (specified by user)
%       Mass       mass of the spaceship in [kg] (specified by user)
%       Location   location of the spacecraft (specified by user from a
%                  menu)
%       Escape     escape velocity for the selected planet in [m/s]
%   OUTPUTS
%       l_g        states which of the listed bodies has the lowest
%                  specific gravity
%       info       states the information about the spacecraft (mass,
%                  location, and escape velocity)

%% Setup

clear
clc
load 'PlanetData.mat' % imports data needed for calculations

%% Calculations

PData(1,:) = PData(1,:).*1000;    % convert from [km] to [m]; saves in 1st row
PData(2,:) = PData(2,:).*(1e+21); % convert from [Yg] to [kg]; saves in 2nd row
Vol = (4/3)*pi.*(PData(1,:).^3);  % volume in [m^3]
Dens = PData(2,:)./Vol;           % density in [kg/m^3]

% creates 4th row
PData(4,:) = Dens./997;                                             % specific gravity; saves in 4th row
Min_d = find(Dens == min(Dens));                                    % finds and stores the spot of the smallest density
Craft = input('What is the name of the spacecraft? ','s');          % asks question and saves user response
Mass = input('Enter the mass of the spaceship in kilograms [kg] '); % asks for mass and saves user response
Location = menu('Where is the spacecraft?',PName);                  % user picks a planet/moon from the options and response is stored
Escape = sqrt(2*PData(1,Location)*PData(3,Location));               % calculates escape velocity

%% Outputs

% Prints the statement filling in with the appropriate values
l_g = fprintf('\n\nThe least dense body is %s with a specific gravity of %.2f.\n',PName{Min_d},PData(4,Min_d));
info = fprintf('\n\n\t\tInformation on %s\n\tSpacecraft mass:\t%.2E grams\n\tLocation:\t\t\t%s\n\tEscape Velocity:\t%.0f m/s\n\n',Craft,Mass*1000,PName{Location},Escape);
