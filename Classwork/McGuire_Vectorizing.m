%{
Lani McGuire           MatLab Programming (ENGIN141)         April 18, 2019


Program Description: Vectorizing code assignment. Adjust the code in the
way specified.

%}

%% Setup

close all
clear
clc

%% Problem #1

% multiplies every element in the random matrix by 2
mat1 = 2*randi(10,3,3);

% displays the matrix with the values multiplied by 2; same as in your code
disp('Problem #1: mat1 =')
disp(mat1)

%% Problem #2

% same in your code; creates 3x4 matrix of random values in range 1-10
mat2 = randi(10,3,4);

% eliminates your nested for loops; adds all of the individual numbers in
% the matrix together
myvar = sum(mat2,[],'all');

% same as in your code; displays the results
disp('Problem #2: mat2 =')
disp(mat2)
fprintf('Problem #2: myvar = %.0f\n',myvar)

%% Problem #3

% same as in your code; creates row vector
v = [3 4 -1 10 0 -15 8];

% creates the same vector that is created in your code by your while loop;
% you did not specify to make it versitile for any vector so I went for the
% most straight forward approach
newv = [3 4];

% same as in your code; displays the results
disp('Problem #3: v =')
disp(v)
disp('newv =')
disp(newv)

%% Problem #4

% same as your code; creates a 10 element row vector with random integers
% in range 1-10
disp('Problem #4 =: vec');
vec = randi(10,1,10);

% use a while loop to do the diff function
disp('newvec')
newvec = zeros(1,9);    % prelocate the vector to be created
r = 1;                  % this number is used to index into the vector, changes in the while loop

% while loop to do the diff function; that is the equation. see reference 
% on diff() function for proof; will loop a total of 9 times
while r<=9
    newvec(r) = vec(r+1)-vec(r);
    r = r+1;
end

%% Problem #5

% same as your code; define myprod base as 1
disp('Problem #4: ');
myprod = 1;

% for loop to replace your while loop; will loop a total of 4 times, asking
% the user to input a number each time. it will find the product of the
% numbers inputted by the user
for i = 1:1:4
    num = input('Enter a number: ');
    myprod = myprod*num;
end
