%{
Lani McGuire           MatLab Programming (ENGIN141)         March 14, 2019


Program Description: Taking rotational kinetic energy as a funtion of
angular velocity, predict the angular velocity of the object in this data
usind a user inputed value; in this ICA it is 10 megajoules, which is
10,000 kilojoules (this data is in kilojoules). Then use a custom function 
to plot the data.


VARIABLES

    INPUTS
        ang_vel   vector containing angular velocity
        rot_KE    vector containing rotational kinetic energy
        MB        calculates m & b value for the line of best fit
        m         m value for theoretical equation
        b         b value for theoreticla equation
        x         x vector for theoretical values
        y         y vector for theoretical values
        u_rotKE   user inputs the rotational kinetic energy (10000 for this
                  assignment)
        u_angve   calculated angular velocity
        eqx       theoretical equation
    OUTPUTS
        graph    figure window and first plot
        graph1   second plot added to first
        ldg      legend handle
%}

%% Setup

close all
clear
clc

%% Inputs

ang_vel = [250 370 430 510 690];         % units -> [rad/s]
rot_KE = [3130 6840 9250 13000 23800];   % units -> [kJ]

% Calculate power line of best fit
MB = polyfit(log10(ang_vel),log10(rot_KE),1);
m = MB(1);        % m value
b = 10^MB(2);     % b value
x = 250:.1:690;   % x values from 250 to 690 at step size .1
y = b * x.^m;     % y values for the theoretical line

u_rotKE = input('\nWhat is the rotational KE in kilojoules [kJ]?\n');

%{
Since,
y = b * x^m
then,
x = (y/b)^(1/m)
where x = angular velocity & y = rotational KE
%}

u_angve = (u_rotKE/b)^m;
eqx = sprintf('y = %.2f x^{%.0f}',b,m);

%% Outputs

% Creates figure and plots theoretical data
graph = lanifig(ang_vel,rot_KE,...
                'Angular Velocity vs. Rotational Kinetic Energy',...   title
                'Angular velocity (\omega) [rad/s]',...                x-axis label
                'Rotational Kinetic Energy');                        % y-axis label

% Plots theoretical data
graph1 = laniplot(x,y);

% Creates legend
lgd = legend('Data Collected',eqx);
lgd.Location = 'Best';
lgd.FontSize = input('What is the font size of the legend?\n');

% Displays answer to question solved by this code
clc
fprintf('\nThe angular velocity at %.0f MegaJoules [MJ] of rotational kinetic energy is %.0f radians per second [rad/s]\n\n',u_rotKE*.001,u_angve)
