%% Lani McGuire          MatLab Programming          February 7, 2019

% Program Description: An unmanned X-43A scramjet test vehicle has achieved
% a maximum speed of Mach number 9.68 in a test flight over the Pacific 
% Ocean. This program will convert the record speed into units of miles per 
% hour.

%% Variables
%    INPUTS
%        rec_mac - record speed as Mach number
%        spd_snd - speed of sound
%        con_s2h - conversion number from mps to mph
%    OUTPUTS
%        rec_mps - record speed in meters per second
%        rec_mph - record speed in miles per hour

%% Setup
clear
clc

%% Input
rec_mac = 9.68; % demensionless
spd_snd = 343; % mps
con_s2h = 2.24; % (mph)/(mps)

%% Calculation
% mach number is the speed of an object divided by the speed of sound, so
% the speed of the object can be found by multiplying the mach number by
% the speed of sound
rec_mph = rec_mac * spd_snd * con_s2h
