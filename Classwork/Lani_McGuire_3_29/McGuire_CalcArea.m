%{
Lani McGuire           MatLab Programming (ENGIN141)         March 31, 2019


Program Description: Calculates the area of a circle, given the radius.


VARIABLES

    INPUTS
        radius   the user inputted radius of the circle
        area     calculated area of a circle
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

radius = input('Enter the radius of the circle: ');

% runs this if the input is negative
if radius<0
    error('Radius cannot be negative')
    
% runs this if the input is 0
elseif radius==0
    error('Radius cannot be zero')
    
% runs this if the input is positive
else
    area = pi*radius^2; % formula for the area of a circle

end

%% Outputs

fprintf('\nFor a circle with radius %.2f, the area is %.2f\n\n',radius,area)
