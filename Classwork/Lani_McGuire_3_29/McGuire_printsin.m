%{
Lani McGuire           MatLab Programming (ENGIN141)         March 31, 2019


Program Description: User imputs an angle an says whether it is degrees or
radians. This program will compute the result and then output it to the
screen.


VARIABLES

    INPUTS
        angle   the angle inputted by the user
        angletypes   an array containing the units for the angle
        flag   hangle for the while loop
        units   user chooses whether the angle is in degrees or radians
        result   the sine of the angle inputted by the user
        typeangle   the units of the angle that the user inputted
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

angle = input('What is the angle?\n');
angletypes = {'radians' 'degrees'};

flag = 0;

%{
This loop calculates the sine and prints it back to the screen. Will loop
until a correct value is entered, or until the user force stops the
program (includes instructions on how to force stop program).
%}

while flag==0
    
    units = input('\nIf the angle is in radians enter ''r''\nIf it is in degrees enter ''d''\n','s');

    % runs this if the input is the letter R, regardless of case;
    % calculates the sine for an angle inputted in radians
    if strcmpi(units,'r')==1
        result = sin(angle);
        typeangle = angletypes{1};
        flag = 1; % ends the while loop
        
    % runs this if the input is the letter D, regardless of case;
    % calculates the sine for an angle inputted in degrees
    elseif strcmpi(units,'d')==1
        result = sind(angle);
        typeangle = angletypes{2};
        flag = 1; % ends the while loop
        
    % runs this if the input is not D or R; prompts the user to choose
    % whether to try again or end the program
    else
        fprintf('\nInvalid Submission.\n')
        fprintf('Press any key to try again.\n')
        fprintf('To end program press (ctrl + C)')
        pause
        clc
        % goes back to the top of the while loop
    end
end    

%% Outputs

fprintf('\nThe sine of %.1f %s is %.4f\n\n',angle,typeangle,result)
