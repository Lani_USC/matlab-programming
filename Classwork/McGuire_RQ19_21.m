%{
Lani McGuire           MatLab Programming (ENGIN141)         April 29, 2019


Program Description: Creates a Bowman sequence the length of the value
chosen by the user


VARIABLES

    INPUTS
        length            the length chosen by the user
        Bowmansequence    the created Bowman sequence
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

% user chooses the length of the Bowman sequence and then the lanibowman()
% function creates the Bowman sequence
length = input('Input integer for the length for the Bowman sequence: ');
Bowmansequence = lanibowman(length);

% if the user chose an invalid number, then a warning is displayed and
% Bowmansequence becomes one number: -1. so if the user chose an invalid
% number, they will be prompted to try again, until a valid number is
% inputted
while Bowmansequence==-1
    pause(5)
    clc
    length = input('Input a valid length: ');
    Bowmansequence = lanibowman(length);
end

%% Function

%{

Function Description: This function creates a Bowman sequence the length of
the number that is put into it and will print the sequence to the screen. 
If that number is invalid, then rather than creating a Bowman sequence, a 
warning will be displayed and -1 will be stored to the variable.


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        N/A
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        num    the length of the Bowman sequence
    OUTPUTS
        B    the bowman sequence

%}

function[B] = lanibowman(num)

% prelocation of bowman function
B = zeros(1,num);

% bowman functions must start with [0 1 2] every time, which is why they
% must be at least 3 long
B(2) = 1;
B(3) = 2;

% if the number is less than 3 a warning is displayed and B becomes -1
if num<3
    warning('The length must be at least 3')
    B = -1;
% if not, the Bowman sequence rule is applied and the rest of the vector is
% filled out accordingly
else
    for x = 4:num
       B(x) = sum([B(x-1) B(x-2) B(x-3)]);
    end    % end of for loop
    fprintf('\nBowman sequence:\n')
    disp(B)
end    % end of if statement

end % end of function
