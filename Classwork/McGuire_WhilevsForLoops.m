%{
Lani McGuire           MatLab Programming (ENGIN141)         April 17, 2019


Program Description: While vs. For loops assignment. Perform each specified
task choosing to do it with a for loop, while loop, nested loop, or no loop
at all.

%}

%% Setup

close all
clear
clc

vec = randi(100,1,10);    % vector to be used in later problems
mat = randi(100,3,4);     % matrix to be used in later problems

%% Problem 1: Solved using no loop

sum = sum(1:1:50);    % finds sum of all integers 1-50

%% Problem 2: Solved using no loop

vec2 = vec+3;    % adds 3 to each element in the vector created in the setup

%% Problem 3: Solved using no loop

mincol = min(mat,[],1);    % finds the minimum number in each column of the matrix created in the setup

%% Problem 4: Solved using 2 seperate for loops

num = zeros(1,5); % prelocation of vector to store user inputed numbers

% prompts user to enter a number 5 times and stores them in their
% respective spots in the prelocated vector
for n = 1:5
    message1 = sprintf('%.0f) Input a number: ',n);    % message statement for the input
    num(n) = input(message1);    % prompts user to input a number
end

average = mean(num); % finds the average of the numbers inputed by the user

% this loop goes through each value in the vector of user inputed values,
% checking to see if they are greater than the average and storing it in
% bigger; could be done in two lines using a logic statement, but for this
% assignment we had to use a while or for loop
bigger = 0;   % contains how many numbers the user inputed that are greater than the average
for x = 1:5
    % when it gets to a number that is greater than the average, it adds 1
    % to bigger, keeping track of how many numbers are greater than the
    % average; in the end bigger will have the number of numbers greater
    % than the average
    if num(x)>average
        bigger = bigger+1;
    end
end

% just prints it to the command window
fprintf('%.0f numbers were greater than the average',bigger)

pause
clc    % clears command window because it is used in the next problem

%% Problem 5: Solved using a while looped nested inside of a for loop

r_int = randi(10); % generates a random integer in the range 1-10

% for loop with nested while loop to prompt user for a positive number and
% displays their number each time for the amount of times equal to the
% randomly generated integer
for a = 1:r_int
    % this only is asked the number of times as r_int, unless the user
    % inputs a number that is not positive
    message2 = sprintf('%.0f) Input a positive number: ',a); % message statement for the input
    u_int = input(message2); % prompts user to input positive number
    while u_int<=0
        % this happens as long as the user inputs a number that is not
        % positive
        warning('That is not a positive number. Try again.')    % gives warning
        u_int = input(message2);    % prompts the user again to input a positive number, still counts for the same number in the for loop
    end
    disp(u_int) % displays the positive integer for the number inputed on this loop of the for loop
end
