%% Lani McGuire       MatLab Programming (Engin 141)      February 10, 2019

% Program Description: Vector and Matrix Problems

%% Setup
clear
clc

%% Assignment

% #1
NegSeq = [-4:1:0] % creates row vector with colon operator
NegSeq = linspace(-4,0,5) % creates row vector with linspace function
DownOdd = [9:-2:5] % creates row vector with colon operator
DownOdd = linspace(9,5,3) % creates row vector with linspace function
UpEven = [4:2:8] % creates row vector with colon operator
UpEven = linspace(4,8,3) % creates row vector with linspace function

% #2
myend = randi([5 9]) % generates random value between 5 and 9 and saves it in myend
v_myend = [1:3:myend] % generates a row vector starting at one and goes to the value in my end at step size 3

% #3
R1 = [-1 5 2] % generates row vector [-1 5 2] and saves it in R1
R2 = [6 -7 11] % generates row vector [6 -7 11] amd saves it in R2
R12 = [R1 R2] % combines R1 with R2 to create one row vector and stores it in R12

% #4
myvec = [-1:.5:1]' % creates a row vector [-1 -.5 0 .5 1] and tranposes it to be a column vector

% #5
colvec = 1:3'
% the apostrophe here means nothing so the colon operator defaults a row
% vector. in order to make a column vector, you have to add square brackets
% and then the apostrophe would transpose the row vector to a column vector
colvec = [1:3]'

% #6
mat = [9 8 7 6;5 4 3 2] % creates 2x4 matrix and stores it in mat
mat(1,:) = [1:4] % replaces 1st row with [1:4]
mat(:,3) = zeros % replaces 3rd column with 0's

% #7
mat_01 = rand(2,3) % create 2x3 matrix of random numbers in range (0,1)
mat_05 = 5 * rand(2,3) % create 2x3 matrix of random numbers in range (0,5)
mat_in = randi([10 50],[2,3]) % create a 2x3 matrix of random integers in range (10,50)

% #8
randreal = 200 * rand(3,5) - 100 % create a 3x5 matrix of random numbers in range (-100,100)
randreal(3,:) = [] % delete the third row
% #9
x = linspace(-pi,pi,20) % create vector of 20 equally spaced points from -pi to pi
y = sin(x) % plugs all numbers into sine

% #10
mysum = sum([2:2:10]) % adds together the values of the row vector [2 4 6 8 10]