%{
Lani McGuire           MatLab Programming (ENGIN141)         March 14, 2019


Program Description: Taking mass as a function of length, predict the mass
of however long the wire is that the user is using; in this ICA it is 1 
kilometer or 1000 meters (this data is in meters). Then use a custom 
function to plot the data.

VARIABLES


    INPUTS
        length     vector containing lengths of same type of wire
        mass       vector containing mass corresponding to the length of 
                   the wire
        MB         finds m & b value for the line of best fit
        m          m value of the equation
        b          b value of the equation
        x          x vector for theoretical values
        y          y vector for theoretical values
        u_length   user inputs the length of the wire (1000 for this
                   assignment)
        u_mass     calculated mass of the wire
        eqx        theoretical equation to display to graph
    OUTPUTS
        graph    figure window and first plot
        graph1   second plot added to first
        ldg      legend handle
%}

%% Setup

close all
clear
clc

%% Inputs

length = [0 5 20 50 100];         % units -> [m]
mass = [0 700 2830 7070 14200];   % units -> [g]

% Calculate linear line of best fit
MB = polyfit(length,mass,1);
m = MB(1);   % m value
b = MB(2);   % b value
x = 0:100;   % x values from 0 to 100 at step size 1
y = m*x+b;   % y values for the theoretical line

u_length = input('\nHow long is the wire in meters [m]?\n');

% y = mx + b: where x = length and y = mass

u_mass = m*u_length+b;
eqx = sprintf('y = %.0f x + %.0f',m,b);

%% Outputs

% Creates figure and plots theoretical data
graph = lanifig(length,mass,...
                'Length of Wire vs. Mass of Wire',...   title
                'Length (L) [m]',...                    x-axis label
                'Mass (M) [g]');                      % y-axis label

% Plots theoretical data  
graph1 = laniplot(x,y);

% Creates legend
lgd = legend('Data Collected',eqx);
lgd.Location = 'Best';
lgd.FontSize = input('\nWhat is the font size for the legend?\n');

% Displays answer to question solved by this code
clc
fprintf('\nThe mass of %.0f kilometer of the same wire is %.0f g.\n\n',u_length*.001,u_mass)
