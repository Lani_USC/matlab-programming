%% Lani McGuire         MatLab Programming ()          March 1, 2019

% Program Description: Create a graph showing the theoretical relationship
% of an ideal gas between pressure (P) and temperature (T).

%% Variable

%   INPUTS
%       P1       pressure in atmoshperes [atm] initial
%       T1       temperature in Kelvin [K] initial
%       n        moles of nitrogen
%       V_l      volume in liters [L]
%       R        ideal gas constant
%       V_conv   number to convert from liters [L] to grams [g]
%       V        volume in grams [g]
%       T        vector containing temperatures in [K]
%       P        vector containing pressures in [atm] corresponding to the
%                temperature
%   OUTPUTS
%       N/A

%% Setup

clear
close all
clc

%% Inputs

P1 = 2.5;            % intial pressure in [K]
T1 = 270;            % intiial temperature in [K]
V_l = 12;            % volume in [L]
V = V_l*1000;        % volume in [g]
R = 0.082057;        % ideal gas constant [(L*atm)/(mol*K)]
n = (P1*V_l)/(R*T1); % finds the moles [mol]
T = 260:.1:360;      % creates vector containg temperatures from 260 K to 360 K at step size .1
P = (n*R.*T)./V_l;   % creates pressure values corresponding to temperature [atm]

%% Outputs

figure('color','w')                                                 % opens figure window
plot(T,P)                                                           % plots temperature on x-axis and pressure on y-axis
axis([260 360 2 3.4])                                               % sets axis bounds [xmin xmax ymin ymax]
xlabel('Temperature [K]','FontSize',12)                             % labels x-axis
ylabel('Pressure [atm]','FontSize',12)                              % labels y-axis
title('Pressure vs. Temperature for Nitrogen in 12L','FontSize',12) % makes title
grid on                                                             % adds grid lines