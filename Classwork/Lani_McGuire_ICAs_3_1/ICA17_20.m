%% Lani McGuire             MatLab Programming ()           March 1, 2019

% Program Description: Plot the folowing data collected during the charging
% of a capacitor.
% +-------------------+---------+---------+---------+---------+---------+
% | Time (t) [s]      |   0.2   |   0.4   |   0.6   |   0.8   |   1.0   |
% +-------------------+---------+---------+---------+---------+---------+
% | Voltage (V) [V]   |  75.9   |  103.8  |  114.0  |  117.8  |  119.2  |
% +-------------------+---------+---------+---------+---------+---------+

%% Variables

% INPUTS
%   Time - vector containing the time in seconds [s}
%   Voltage - vector containing the voltage in Volts [V]
% OUTPUTS
%   N/A

%% Setup

clear
close all
clc

%% Inputs

Time = [0.2 0.4 0.6 0.8 1.0];               % times in units [s]
Voltage = [75.9 103.9 114.0 117.8 119.2];   % voltages in units [V]

%% Plot

figure('color','w')                                             % opens figure on white background
plot(Time,Voltage,'b.','MarkerSize',12)                         % creates graph with blue dots sized at 12
title('Voltage of Charging Capacitor Over Time','FontSize',12)  % makes title
xlabel('Time (t) [s]','FontSize',12)                            % titles x axis
ylabel('Voltage (V) [V]','FontSize',12)                         % titles y axis
axis([0 1.2 0 120])                                             % sets axis bounds
grid on
