%% Lani McGuire          MatLab Programming          February 7, 2019

% Program Description: This program will ask the user a series of questions
% about the volume, temperature, and pressure in a container and then will
% calculate the mass of oxygen gas in the container. IT IS NOT DEBUGGED, SO
% PLEASE ONLY INPUT NUMBERS AS REQUESTED!

%% Variables
%   INPUTS
%       mol - molecular weight of oxygen
%       R - gas constant
%       vol - volume of the container in gallons
%       temp - temperature in the container in Celsius
%       P - pressure in the container in atmospheres
%       V_conv - conversion number between gallons and liters
%       V - volume of the container in liters
%       T - temperature of the container in Kelvin
%       n - moles of oxygen present
%   OUTPUTS
%       gram_oxygen - mass of oxygen gas in the container in grams

%% Setup
clear
clc

%% Inputs
mol = 32; % units of g/mol
R = 0.08206; % units of (L*atm)/(mol*K)
V_conv = 3.785; % units of L/gal
% Questions
vol = input('Volume. Enter a number in unit gallons. ');
temp = input('Temperature. Enter number in unit Celsius. ');
P = input('Pressure. Enter number in unit atmospheres. ');

%% Calculations
V = vol * V_conv; % converts gallons to liters
T = temp + 273; % converts Celsius to Kelvin
n = (P*V)/(R*T); % this is simply the ideal gas law; returns in unit mol
gram_oxygen = n * mol % converts moles of oxygen to grams of oxygen
