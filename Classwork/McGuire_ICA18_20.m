%{
Lani McGuire           MatLab Programming (ENGIN141)         March 31, 2019


Program Description: ICA 18-20. Generates a menu of vehicle options for the
user to to select from and then a menu for the user to choos the type of
transmission. Then it outputs their selection to them.


VARIABLES

    INPUTS
        Vehicles - array containing the vehicle types
        Transmissions - array containing the tranmsission types
        UserChoice1 - user selects vehicle from menu
        UserVehicle - the user selected vehicle
        UserChoice2 - user selects transmission from menu
        UserTransmission - the user selected transmission
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

Vehicles = {'car' 'truck' 'SUV' 'motorcycle' 'other'};
Transmissions = {'manual' 'automatic'};
UserChoice1 = menu('Select your vehicle:',Vehicles);

% if the user chooses 'other', then this runs
if UserChoice1==5
    UserVehicle = input('Type your vehicle:\n','s');

% if they pick any of the other options, then this runs
else
    UserVehicle = Vehicles{UserChoice1};
    
end

UserChoice2 = menu('Select your transmission:',Transmissions);
UserTransmission = Transmissions{UserChoice2};

%% Outputs

fprintf('\nYou selected a %s with %s transmission.\n\n',...
    UserVehicle,...
    UserTransmission)
