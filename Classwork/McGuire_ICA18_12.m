%{
Lani McGuire           MatLab Programming (ENGIN141)         March 26, 2019 


Program Description: ICA 18-12 with a twist. Asks user to input a matrix of 
any size with at least two columns. Will give an error if there are not at 
least two columns and prompt the user to input another matrix. Create a new 
matrix with the max and min values from each.


VARIABLES

    INPUTS
        flag         trigger variable for the while loop
        mat          the user inputed matrix
        matr         the amount of rows in the user inputed matrix
        matc         the amount of columns in the user inputed matrix
        col          the minimum number of columns that the user can input
        my_oddmat    matrix containing the odd columns from mat
        my_evenmat   matrix containing the even columns from mat
        min_odd      column vector containing the min value of each row of
                     my_oddmat, which contains only the odd columns
        max_even     column vector containing the max values of each row of
                     my_evenmat, which contains only the even columns
        my_mat       matrix where the first column contains the contents of
                     min_odd and the second column contains the contents of 
                     max_even
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

flag = 0;

while flag==0
    mat = input('Please enter a matrix with at least two columns, in the form\n[X1 Y1;X2 Y2; . . . ]:\n');
    [matr,matc] = size(mat);
    col = 2;
    % if the inputed matrix has at least 2 columns this happens
    if matc >= col
        my_oddmat = mat(:,1:2:matc);
        my_evenmat = mat(:,2:2:matc);
        min_odd = min(my_oddmat,[],2);
        max_even = max(my_evenmat,[],2);
        my_mat = [min_odd max_even];
        flag = 1;                        % this will end the while loop
    % if not, this happens
    else
        fprintf('Invalid matrix submission\n')
        fprintf('To cancel code, press (ctrl + C).\nTo try again press any key.\n')
        pause
        clc
    end
end
    
%% Outputs

%{
The following for loops will start at 1 and will repeat until the number of 
rows that the user inputed. Each repetition will go up an integer value,
filling in data appropriately.
%}

fprintf('\nThe smallest value found in the odd-numbered columns:\n')
for i = 1:matr
    fprintf('\tRow %i = %.0f\n',i,my_mat(i,1))
end

fprintf('\nThe largest value found in the even-numbered columns:\n')
for i = 1:matr
    fprintf('\tRow %i = %.0f\n',i,my_mat(i,2))
end
