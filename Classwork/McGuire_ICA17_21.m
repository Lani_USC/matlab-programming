%% Lani McGuire        MatLab Programming (ENGIN141)      March 10, 2019

%{

Program Description: Graph the data from ICA 17-21 and create a linear 
trendline for each type of electrical consumption (Incadenscent, CFL, & 
LED).

%}

%% Variables

%{
    INPUTS
        LF       vector containing luminous flux (LF) values
        INC      vector containing electrical consumption (EC) values for 
                 the incadescent techonology, corresponding to flux values
        CFL      vector containing electrical consumption (EC) values for 
                 the CFL techonology, corresponding to flux values
        LED      vector containing electrical consumption (EC) values for 
                 the LED techonology, corresponding to flux values
        T_LF     vector flux values for theoretical graph, from 80-1400
        I_LF     vector containing flux values adjusted to be the same 
                 length as and correspond to the incandescent vector
        blk1     finds the empty spots in the INC vector
        MB_INC   finds the value of m and b for the incandescent equation
        m1       m value for the incandenscent equation
        b1       b value for the incandenscent equation
        T_INC    theoretical equation for the incandescent techonology
        C_LF     vector containing flux values adjusted to be the same 
                 length as and correspond to the CFL vector
        blk2     finds the empty spots in the CFL vector
        MB_CFL   finds the value of m and b for the CFL equation
        m2       m value for the CFL equation
        b2       b value for the CFL equation
        T_CFL    theoretical equation for the CFL techonology
        L_LF     vector containing flux values adjusted to be the same 
                 length as and correspond to the LED vector
        blk3     finds the empty spots in the LED vector
        MB_LED   finds the value of m and b for the LED equation
        m3       m value for the LED equation
        b3       b value for the LED equation
        T_INC    theoretical equation for the LED techonology
%}
%{
    OUTPUTS
        G        grid
        T        title
        X        x-axis label
        Y        y-axis label
        te_inc   theoretical equation for the incandescent technology
        te_cfl   theoretical equation for the CFL technology
        te_led   theoretical equation for the LED technology
        lgd      legend
%}

%% Setup

close all
clear
clc

%% Inputs

% Given Data

% NaN acts as a placeholder, will be removed later
LF = [80 200 400 600 750 1250 1400]; % units -> [lm]
INC = [16 NaN 38 55 68 NaN 105];     % units -> [W]
CFL = [NaN 5 10 NaN 18 27 33];       % units -> [W]
LED = [1.7 3 6 9 12 NaN NaN];        % units -> [W]

% Theoretical Data

T_LF = 80:1400;

% Incandescent
I_LF = LF;                    % copies flux value vector
blk1 = find(isnan(INC));      % finds all the NaN's (placeholder's) positions in the INC vector
INC([blk1(1) blk1(2)]) = [];  % deletes the NaNs from INC vector
I_LF([blk1(1) blk1(2)]) = []; % deletes the numbers from the flux vector that correspond to the NaNs in the INC vector
MB_INC = polyfit(I_LF,INC,1); % finds the m and b values using the modified vectors
m1 = MB_INC(1);
b1 = MB_INC(2);
T_INC = m1 * T_LF + b1;

% CFL
C_LF = LF;                    % copies flux value vector
blk2 = find(isnan(CFL));      % finds all the NaN's (placeholder's) positions in the CFL vector
CFL([blk2(1) blk2(2)]) = [];  % deletes the NaNs from CFL vector
C_LF([blk2(1) blk2(2)]) = []; % deletes the numbers from the flux vector that correspond to the NaNs in the CFL vector
MB_CFL = polyfit(C_LF,CFL,1); % finds the m and b values using the modified vectors
m2 = MB_CFL(1);
b2 = MB_CFL(2);
T_CFL = m2 * T_LF + b2;

% LED
L_LF = LF;                    % copies flux value vector
blk3 = find(isnan(LED));      % finds all the NaN's (placeholder's) positions in the LED vector
LED([blk3(1) blk3(2)]) = [];  % deletes the NaNs from LED vector
L_LF([blk3(1) blk3(2)]) = []; % deletes the numbers from the flux vector that correspond to the NaNs in the CFL vector
MB_LED = polyfit(L_LF,LED,1); % finds the m and b values using the modified vectors
m3 = MB_LED(1);
b3 = MB_LED(2);
T_LED = m3 * T_LF + b3;

%% Outputs

% Plots data

figure('Color','w')

% Incandescent plot
plot(I_LF,INC,'bo','MarkerSize',8)  % given data
hold on
plot(T_LF,T_INC,'b-','LineWidth',2) % theoretical data

% CFL plot
hold on
plot(C_LF,CFL,'rs','MarkerSize',8,'MarkerFaceColor','r') % given data
hold on
plot(T_LF,T_CFL,'r:','LineWidth',2)                      % theoretical data

% LED plot
hold on
plot(L_LF,LED,'kd','MarkerSize',8)   % given data
hold on
plot(T_LF,T_LED,'k--','LineWidth',2) % theoretical data

% Plot details

axis([0 1600 0 120])

% Grid
grid on
set(gca,'xtick',0:200:1600,'ytick',0:10:120)
G = gca;
G.FontSize = 10;

% Title
T = title('Electrical Consumption vs. Luminous Flux');
T.FontSize = 16;

% X-axis
X = xlabel('Luminous Flux (LF) [lm]');
X.FontSize = 14;

% Y-axis
Y = ylabel('Electrical Consumption (EC) [W]');
Y.FontSize = 14;

% Equations
te_inc = sprintf('y = %.3f x + %.2f',m1,b1);
te_cfl = sprintf('y = %.3f x + %.2f',m2,b2);
te_led = sprintf('y = %.3f x + %.2f',m3,b3);

% Legend
lgd = legend('Incandescent Experimental',te_inc,'CFL Experiemental',te_cfl,'LED Experimental',te_led);
lgd.FontSize = 10;
lgd.Location = 'northwest';
