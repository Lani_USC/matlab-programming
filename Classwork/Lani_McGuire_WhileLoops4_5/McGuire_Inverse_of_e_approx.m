%{
Lani McGuire           MatLab Programming (ENGIN141)         April 7, 2019


Program Description: Number 2 of "While Loops 4/5" assignment. Loops
through values of n trying to get to an approximation where the difference
between the approximation and the actual is less than 0.0001. The target is
1/e. I used only interger values in this assignment.


VARIABLES

    INPUTS
        inv_e     the inverse of the exponential (e) variable
        n         the n value used in the equation for the approximation
        approx    the approximation caluculated using the n value
        diff      the difference between the actual and the approximation
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

inv_e = exp(-1);
n = 1;
approx = (1-(1/n))^n; % equation
diff = inv_e-approx;

while abs(diff)>=.0001
    n = n+1;
    approx = (1-(1/n))^n;
    diff = inv_e-approx;
end

%% Outputs

fprintf('\nThe actual value of e^{-1} is %f.\n',inv_e)
fprintf('The approximation %.4f is given by %.0f.\n\n',approx,n)
