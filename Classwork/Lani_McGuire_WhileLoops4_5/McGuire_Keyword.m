%{
Lani McGuire        MatLab Programming (ENGIN141)      Date


Program Description: The user is prompted to input the keyword. They will
continue to be prompted until they get it right or manually terminate the
program.


VARIABLES

    INPUTS
        keyword      my chosen keyword
        user_word    the user inputed word
        answer       the answer (1 if their guess is correct, 2 if its
                     incorrect)
    OUTPUTS
        N/A
%}

%% Setup

close all
clear
clc

%% Inputs

keyword = 'Jesus';
user_word = input('What is the keyword?\n','s');
answer = strcmpi(keyword,user_word);

while answer==0
    warning('Incorrect keyword')
    fprintf(2,'Try again.\n\n')
    pause(1)
    user_word = input('What is the keyword?\n','s');
    answer = strcmpi(keyword,user_word);
end

%% Outputs

fprintf('CORRECT!!!\n')
