%{
Lani McGuire           MatLab Programming (ENGIN141)         April 8, 2019


Function Description: Creates a multiplication table from the given amount
of rows and columns.


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        r    variable for the for loop that creates the rows
        c    variable for the for loop that creates the columns
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        rows       number of rows for the table to have
        columns    number of colums for the table to have
    OUTPUTS
        matrix    multiplication table created

%}

function[matrix] = McGuiremulttable(rows,columns)

matrix = zeros(rows,columns);
for r = 1:1:rows
    for c = 1:1:columns
        matrix(r,c) = r*c;
    end
end

disp(matrix)

end
