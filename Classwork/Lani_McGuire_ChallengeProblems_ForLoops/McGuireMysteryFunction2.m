%{
Lani McGuire           MatLab Programming (ENGIN141)         April 8, 2019


Function Description: Does the exact same thing as the
McGuireMysteryFunction() function, but in a much clearer and simpler way.
Outputs a matrix of +'s. The amount of rows will directly correspond to the 
number that goes into the function. The amount of columns will be 2 times 
the number inputed.

VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        n    variable for 1st for loop
        x    variable for 2nd for loop
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        number    any positive integer
    OUTPUTS
        N/A
%}

function McGuireMysteryFunction2(number)

% Generates error if number is negative; works otherwise
if number<0
    error('This function doesn''t work for negative values.')
else
    for n = 0:1:number-1
        for x = 0:1:number-1
            fprintf('++')
        end
        % loops through creating the length of the matrix and then goes
        % down a line and does it again and goes down a line, looping this
        % process whatever number of times
        fprintf('\n')
    end
end

end
