%{
Lani McGuire           MatLab Programming (ENGIN141)         April 8, 2019


Program Description: Creates a multiplication table a size of the user's
choosing. Multiplication table is created by the function:

McGuiremulttable()
Creates a multiplication table from the given amount of rows and columns.


VARIABLES

    INPUTS
        rows       user chosen amount of rows
        columns    user chosen amount of columns
        mult       generated multiplication table
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

fprintf('This will generate a multiplication table.\n')
rows = input('\nHow many rows do you want?\n');
columns = input('\nHow many columns do you want?\n');

%% Outputs

clc
mult = McGuiremulttable(rows,columns); % creates multiplication table
