%{
Lani McGuire           MatLab Programming (ENGIN141)         April 8, 2019


Function Description: Outputs a matrix of +'s. The amount of rows will
directly correspond to the number that goes into the function. The amount
of columns will be 2 times the number inputed.


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        vector    the vector created by the magic number
        n         variable for the vector for loop
        matrix    the matrix created by the magic number and vector
        x         variable for the matrix for loop
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        number    any positive integer
    OUTPUTS
        N/A

NOTE: Will only generate an error if a negative number is inputed.
%}

function McGuireMysteryFunction1(number)

% Generates error if number is negative; works otherwise
if number<0
    error('This function doesn''t work for negative values.')
else
    % For loop that creates the vector
    vector = []; % Creates empty vector
    for n = 0:1:number-1
        % if the number is 0, then we use an empty vector
        % will add '++' to the vector however many times the number
        % represents
        vector = [vector '++']; 
    end % ends vector for loop
    
    % For loop that prints matrix of +'s
    for x = 0:1:number-1
        % if number is 0, an empty matrix is made
        % will add the vector created above to the bottom of the matrix
        % however many times the number represents
        disp(vector)
    end % ends matrix for loop
end

end % end of function
