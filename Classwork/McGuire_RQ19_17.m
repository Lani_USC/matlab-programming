%{
Lani McGuire           MatLab Programming (ENGIN141)         April 10, 2019


Program Description: Program for control center of a robot tractor. Will
plant and till crops in circular fields. Will do a double-wide spiral
inward, followed by a second outward spiral exactly halfway between the
lines of the inward spiral. This code will genereate that spiral pattern to
the figure window.


VARIABLES

    INPUTS
        radius    user inputed radius
        A         user inputed number of spirals per arm
        theta     angles 0 to 2*pi
        R         vector for changing radius values
        Xin       x values for the curve going into the center
        Yin       y values for the curve going into the center
        Xout      x values for the curve going out from the center
        Yout      y values for the curve going out from the center
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

radius = input('Input the outer most radius of the spiral: ');
A = input('Input how many spirals you desire for one arm to have: ');

%{
Notice the length of theta is the same as the length of R. They are both 
really large numbers (10,000) for many data points so the curve is smooth.
%}
theta = linspace(0,2*pi,10000);
R = linspace(radius,0,10000);

% prelocation of vectors
Xin = zeros(1,10000);
Yin = zeros(1,10000);
Xout = zeros(1,10000);
Yout = zeros(1,10000);

%{
In order for the spiral to be seen, the radius (R) has to change with the
angle (theta), at the same rate, which is why their vectors have to be the 
same length.
%}

for n = 1:1:10000

    %{
    Equations to change polor coordinates to rectangular:
    X = R*cos(A*theta)
    Y = R*sin(A*theta)
    
    R controls how wide the curve is. A controls the amount of spirals, but
    you only see the spiral when R is changing. This for loop accomplishes
    this, making sure that the radius (R) changes at the same rate as the 
    angle (theta).
    %}
    
    Xin(n) = R(n)*cos(A*theta(n));
    Yin(n) = R(n)*sin(A*theta(n));
    
    Xout(n) = -R(n)*cos(A*theta(n));
    Yout(n) = -R(n)*sin(A*theta(n));

end

%% Outputs

% plots the curve going into the center; it's blue
plot(Xin,Yin,'b')

hold on

% plots the curve going out from the center; it's red
plot(Xout,Yout,'r')
