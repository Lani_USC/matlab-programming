%% Lani McGuire        MatLab Programming (ENGIN141)      February 25, 2019

% Program Description: Store data about cars, find how many gallons used
% and the gas mileage. Then calculate how much carbon dioxide is produced.
% Given data shown below.

% +-------------+--------------+-------------+-----------+---------------+
% |             |   Gas Mileage|  Gas Mileage|    Sticker|      Fuel Tank|
% |    Car Model| Highway (mpg)|   City (mpg)|  Price ($)| Capacity (gal)|
% +-------------+--------------+-------------+-----------+---------------+
% | Toyota Prius|            50|           54|      23475|           11.3|
% +-------------+--------------+-------------+-----------+---------------+
% |  Ford Escape|            30|           23|      21435|           15.7|
% +-------------+--------------+-------------+-----------+---------------+
% |   Ford Focus|            38|           26|      17950|           13.9|
% +-------------+--------------+-------------+-----------+---------------+

%% Variables

%   INPUTS
%       Cars           1x3 cell array containing car models
%       CarData        3x4 matrix containing data from given table; the 
%                      rows correspond to the car models; the first column 
%                      is highway gas mileage, the second is city gas 
%                      mileage, the third is sticker price, the fourth is 
%                      the capacity of the fuel tank
%       MILES          2 element vector containing highway and city miles
%       POUNDSCO2      pounds of CO2 per gallon burned
%       CarType        user will select car type from a menu
%       CarModel       model of the car selected by user
%       GallonsofGas   how many gallons of gas the car chosen uses
%       CO2gas         how much CO2 the selected car burns
%   OUTPUTS
%       N/A


%% Setup

close all
clear
clc

%% Initial Data

% defining intial values --> see variables section
Cars = {'Toyota Prius' 'Ford Escape' 'Ford Focus'};
CarData = [50 54 23475 11.3;30 23 21435 15.7;38 26 17950 13.9]; % units -> column 1: mpg | column 2: mpg | column 3: $ | column 4: gallons
MILES = [320 125];                                              % units -> miles
POUNDSCO2 = 20;                                                 % units -> [lb/g]

%% User Selections

% see variables section
CarType = menu('Select your car.',Cars);
CarModel = Cars{CarType};
GallonsofGas = (MILES(1)/CarData(CarType,1))+(MILES(2)/CarData(CarType,2)); % units -> gallons
CO2gas = POUNDSCO2*GallonsofGas;                                            % units -> pounds

%% Output

fprintf('\nCar type chosen is %s.\n\nHighway and City gas mileage is %.0f and %.0f mpg.\nGallons of gas burned is %.1f.\nPounds of CO2 produced is %.2E.\n\n',CarModel,CarData(CarType,1),CarData(CarType,2),GallonsofGas,CO2gas)
