%% Lani McGuire         MatLab Programming ()           Februaruy 22, 2019

% Program Description: Review problems for test 1

%% Part a

clear
close all
clc

K = 1950;
C = K - 273;
F = (9/5) * C + 32;
R = K * (9/5);

Temp = {'Kelvin' 'Celsius' 'Fahrenheit' 'Rankine';K C F R};

%% Part b

% see file 'Test1_PQuestions'

%% Part c

clear
close all
clc

Cell4 = {'2' '4' '6' '8'};
Numbers = [5 10 15 20;1 2 4 8;3 6 9 12;4 8 12 16];
Choice = menu('Select a number',Cell4);
Choice1 = str2num(Cell4{Choice});
Choice = menu('Select a number',num2cell(Numbers(1,:)));
Choice2 = Numbers(1,Choice);
fprintf('\n\nThe two numbers that we''re multiplying are %.0f and %.0f\n\nThe product = %.0f\n\n',Choice1,Choice2,Choice1*Choice2)
Cell4Length = length(Cell4);
Cell4{5} = input('Type the name.\n','s');
