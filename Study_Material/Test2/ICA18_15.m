%{
Lani McGuire           MatLab Programming (ENGIN141)         April 3, 2019


Program Description: ICA 18-15. User selects teh color of the traffic light
and will be told to continue, slow down, or stop, based on their selection
in the menu.


VARIABLES

    INPUTS
    OUTPUTS

%}

%% Setup

close all
clear
clc

%% Inputs

Color = menu('Color of the Light','Green','Yellow','Red');

%% Outputs

% if red is selected OR if the menu is closed
if Color==3||Color==0
    fprintf(2,'STOP!!')

% if green is selected
elseif Color==1
    fprintf('Continue.')

% if yellow is selected
else
    fprintf('Slow down.')
end
