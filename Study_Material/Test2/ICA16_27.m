%{
Lani McGuire           MatLab Programming (ENGIN141)         April 1, 2019


Program Description: ICA 16-27. Plots data for the amount of exoplanets
discovered over time since 1988.


VARIABLES

    INPUTS
        time          vector containing time values
        exoplanets    vector containing the amount of exoplanets discovered
                      corresponding to the years
        graph1        creation of figure window, and plot of experimental data
        m             m value for the line of best fit
        b             b value for the line of best fit
        bestline      equation for the line of best fit
        graph2        plot for line of best fit
        lgd           handle for the legend
        y             user chooses how many exoplanets are discovered
        x             calculates how many years after 1988 the amount of
                      exoplanets inputed will be discovered
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

time = [0 4 8 11 14 17 21 24 27];
exoplanets = [1 5 13 31 96 187 338 881 2078];

%% Graphs

graph1 = lanifig(time,exoplanets,...
             'Discovery of Exoplanets Over Time',...
             'Year (Y) [yr]',...
             'Exoplanets (P)');

% Plots theoretical data
[m,b,bestline,graph2] = lanibestfit(time,exoplanets);

% Creates legend
lgd = legend('Data Collected',bestline);
lgd.Location = 'Best';
lgd.FontSize = input('\nWhat is the font size for the legend?\n');

%% More Calculations

y = input('How many exoplanets are discovered?\n');
x = log(y/b)/m;

%% Outputs

fprintf('Theoretically, in year %.0f, %.0f exoplanets are expected to be discovered.\n\n',1988+x,y)
