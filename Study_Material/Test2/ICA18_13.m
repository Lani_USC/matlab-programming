%{
Lani McGuire           MatLab Programming (ENGIN141)         April 3, 2019


Program Description: ICA 18-13. User selects from a menu and the program
will determine what they are based on their response.


VARIABLES

    INPUTS
        Status    user selects their class from a menu and the position is
                  stored
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

%% Inputs

Status = menu('Class','Freshmen','Sophomore','Junior','Senior');

%% Outputs

% if freshmen is selected
if Status==1
    fprintf('\nYou are a new student.\n\n')
    
% if the user closes the window without making a selection
elseif Status==0
    fprintf('\nYou did not make a selection.\n\n')
    
% if any of the other options are selected
else
    fprintf('\nYou are a continuing student.\n\n')
end
