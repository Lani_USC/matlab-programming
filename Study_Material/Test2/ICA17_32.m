%{
Lani McGuire           MatLab Programming (ENGIN141)         April 3, 2019


Program Description: ICA 17-32. Graphs the data for peoples delay at
stoplights at different intersection. Also plots the line of best fit for
each intersection.


VARIABLES

    INPUTS
        t
        int1
        int2
        int3
        graph1
        m1
        b1
        eqn1
        plot2
        plot3
        m2
        b2
        eqn2
        plot4
        plot5
        m3
        b3
        eqn3
        plot6
        lgd
    OUTPUTS

NOTE: This program contains three custom functions:

        lanifig()
        Creates figure window and plots previously defined data.

        laniplot()
        Will add additional plots to an already existing one. Designed to 
        work with the lanifig() function. 

        lanibestfit()
        Will calculate and graph a line of best fit (liner, power, or 
        exponential) based on what the user chooses. Works best if the user 
        knows, before hand, what type of line of best fit to select.

%}

%% Setup

close all
clear
clc

%% Inputs

t = [2 5 8 11];
int1 = [.05 .1 .3 .4];
int2 = [.1 .5 1 1.3];
int3 = [.5 1.5 2.5 3.1];

%% Outputs

fig1 = lanifig(t,int3,...
                 'Violation Time at Intersections after Installation',...
                 'Time After Installation (t) [months]',...
                 'Violation Time (V) [s]');

[m1,b1,eqn1,plot2] = lanibestfit(t,int3);
plot3 = laniplot(t,int2);
[m2,b2,eqn2,plot4] = lanibestfit(t,int2);
plot5 = laniplot(t,int1);
[m3,b3,eqn3,plot6] = lanibestfit(t,int1);

axis([1 12 0 4])

lgd = legend('Intersection 1',eqn1,...
             'Intersection 2',eqn2,...
             'Intersection 3',eqn3);
lgd.Location = 'Best';
lgd.FontSize = input('\nWhat is the font size for the legend?\n');
