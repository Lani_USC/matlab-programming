%{
Lani McGuire            MatLab Programming (ENGIN141)          May 28, 2019


Program Description: Final Exam. Prompts user to input a temerature value
and a relative humidity percent. The code will then determine the warning
to issue and displays the warning. Then it plots the point on the graph.
This will repeat until the user is satisfied.


VARIABLES

    INPUTS
        X1           x values for line between caution and danger
        Y1           y values for line between caution and danger
        X2           x values for line between danger and extreme danger
        Y2           y values for line between danger and extreme danger
        choice       number value corresponding to if the user chooses to
                     continue or not
        usertemp     user inputted temperature in Fahrenheit
        userhumid    user inputted percent relative humidity
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

global X1
global Y1
global X2
global Y2

%% Inputs - Global Variables

X1 = linspace(85,98,100);
Y1 = linspace(100,40,100);
X2 = linspace(89,108,100);
Y2 = linspace(100,40,100);

%% Plotting

laniindexgraph(X1,Y1,X2,Y2) % function description below

choice = 1;
while choice==1
    clc
    
    usertemp = input('Please input the temperature in Fahrenheit: ');
    userhumid = input('Please input the relative humidity as a percent: ');
    
    warn = HeatWarning(usertemp,userhumid); % function description below
    
    % only plots data that will fit on the graph. doesn't plot anything
    % outside the range of the initial graph. for x that is 80-110 and for
    % y that is 40-100
    if strcmp(warn,'Caution')||strcmp(warn,'Danger')||strcmp(warn,'Extreme Danger')
        fprintf('\nFor the temperature %.1f and relative humidity %.1f,\n',usertemp,userhumid)
        fprintf('the heat index is %s.\n\n',warn)
        % prevents things from outside the range from being plotted
        if usertemp<110 && userhumid>40
            hold on
            plot(usertemp,userhumid,'k.','MarkerSize',20)
        end
    end
    choice = menu('Would you like to try another data point?','Yes','No');
    
end

%% Functions

function laniindexgraph(x1,y1,x2,y2)
%{
Function Description: Plots the base graph, the one with the line
boundaries, just without any data points. Also  labels the sections


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        N/A
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        x1    x values for first line
        y1    y values for first line
        x2    x values for second line
        y2    y values for second line
    OUTPUTS
        N/A

%}

figure('Color','w')

% plots first line
plot(x1,y1,'b-')
hold on
% plots second line
plot(x2,y2,'r-')

grid on
axis([80 110 40 100])

title('Heat Index',...
    'FontSize',16)
xlabel('Temperature (T) [F]',...
    'FontSize',12,...
    'FontWeight','bold')
ylabel('Relative Humidity (RH) [%]',...
    'FontSize',12,...
    'FontWeight','bold')

% labels
text(85,50,'Caution')
text(96,60,'Danger')
text(98,80,'Extreme Danger')

end

function[WARNING] = HeatWarning(Temperature,Humidity)
%{
Function Description:


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        N/A
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        Temperature    temperature in Fahrenheit
        Humidity       humidity as a percent
    OUTPUTS
        WARNING        corresponding warning generated

NOTE: X1, Y1, X2, and Y2 are global variables and have been defined above
already.
%}

global X1
global Y1
global X2
global Y2

% finds equation for line 1
line1 = polyfit(X1,Y1,1);
M1 = line1(1);
B1 = line1(2);
index1 = M1*Temperature+B1;

% finds equation for line 2
line2 = polyfit(X2,Y2,1);
M2 = line2(1);
B2 = line2(2);
index2 = M2*Temperature+B2;

% generates warning corresponding to temperatures and relative humidity
% values
if Humidity<0 || Humidity>100
    fprintf(2,'\nInvalid Humidity Value\n\n')
    WARNING = sprintf('Invalid Humidity Value');
elseif Temperature<80
    fprintf(2,'\nLow Temperature: not classified\n\n')
    WARNING = sprintf('Low Temperature: not classified');
elseif Temperature<98 && Humidity<index1
    fprintf(2,'\nCaution\n\n')
    WARNING = sprintf('Caution');
elseif Temperature<108 && Humidity<index2
    fprintf(2,'\nDanger\n\n')
    WARNING = sprintf('Danger');
else
    fprintf(2,'\nExtreme Danger\n\n')
    WARNING = sprintf('Extreme Danger');
end

end
