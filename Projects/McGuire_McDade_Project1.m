%% Lani McGuire & Caitlin McDade      MatLab Programming ()      2/20/2019

% Program Description: Calculate the volume of a reaction vessel in cubic
% centimeters.

%% Variables
%   INPUTS
%       GasData       a 2x4 matrix (1st row contains temperature in [C]; 
%                     2nd row contains pressure in [atm])
%       Options       cell array containing the quantities for the number 
%                     of moles
%       molesChoice   creates menu containing values from 'Options' and
%                     stores response as a scalar
%       moles         the user selected value of moles
%       R             ideal gas constant
%       P             pressure values in [atm]
%       n             number of moles in [mol]
%       T             temperature in Kelvin [K]
%       V             volume in liters [L]
%       volume        the volume in [cm^3]
%   OUTPUTS
%       N/A

%% Setup

clear
clc

%% Given

GasData = [116 234 310 152;10 55 88 96]; % first row is temperature value [C]; second row is pressure value [atm]

%% Input

Options = {10 100 1000 100000};                                        % number of moles
molesChoice = menu('Select a value for the number of moles.',Options); % user selects mole value from list; stores number of the place where that number is
mole = Options{molesChoice};                                           % indexes into the the options vector in the spot chosen by the user

%% Calculations

R = 0.082057;           % units [(L*atm)/(mol*K)]
P = GasData(2,:);       % defines P as the pressure in [atm]
n = mole;               % defines n as the number of moles in [mol]
T = GasData(1,:) + 273; % converts temperature from [C] to [K]
V = (n*R.*T)./P;        % calculates the volume [L]
volume = V.*1000;   % converts from [L] to [cm^3]
GasData(3,:) = volume;  % creates 3rd row in GasData

%% Output
fprintf('\n\nThe volume of the reaction vessel at %.0f [C] and %.0f [atm] is %.2E [cm^3].\n\n',GasData(1,2),GasData(2,2),GasData(3,2))
