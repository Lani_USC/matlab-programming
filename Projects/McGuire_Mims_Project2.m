%{
Lani McGuire % Collen Mims   MatLab Programming (ENGIN141)   March 20, 2019


Program Description: Reads an excel file containing data on M&Ms. Will
organize data and plot the averages. Then it recounts the average number of
M&Ms per bag, the average mass per bag, the color that appears the most,
and the color that appears the least.


VARIABLES

    INPUTS
        MMS   (matrix) the data from the excel file
        A     (num) the average number per bag
        AM    (num) the average mass per bag
        MXC   (str) the color that appears the most
        MNC   (str) the color that appears the least
    OUTPUTS
        N/A


NOTE: The function mms() is a custom function designed to calculate the
average amount of each color, the average amount of M&Ms per bag, the
average mass per bag, the color that appears the most, the color that
appears the least, and will create a bar graph of the averages of each 
color. The function takes in the matrix of M&M values and outputs the 
average number of M&Ms per bag, the average mass per bag, the color that 
appears the most, the color that appears the least, and the bar graph.

%}

%% Setup

clear
close all
clc

%% Inputs

MMS = xlsread('CandyCount.xlsx'); % imports and reads the M&Ms data matrix

[A,AM,MXC,MNC] = mms(MMS); 

%% Outputs

fprintf('\nM&M analysis by Color\n\t\t\t\t\t\t\t\t\tOverall\nAverage # per bag:\t\t\t\t\t5%.1f\nAverage mass per bag:\t\t\t\t%.1f\nColor that appears the most:\t\t%s\nColor that appears the least:\t\t%s\n\n',A,AM,MXC,MNC)

