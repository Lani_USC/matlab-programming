%{
Lani McGuire & Collen Mims    MatLab Programming (ENGIN141)    May 6, 2019


Program Description: Review Question 19-22. Create a program that would
read a device temperature and tell the user what mode it was in, as well as
plot it.


VARIABLES

    INPUTS
        data          the data pulled from the excel file
        tempascend    sorts the rows in ascending order by the first column
        data2         array containing the temperatures and illuminations 
                      for the user to choose from
        x             handle for the for loop
        temps         user chosen temperature
        illus         user chosen illumination corresponding to temperature
        flag1         handle for while loop 1
        flag2         handle for while loop 2
        usertemp      user chooses value from menu
        illu          illumination value chosen by user
        temp_K        temperature chosen by the user converted to Kelvin
        stat          the status
        flag3         handle for while loop 3
        line_x        boundary line x values
        line_y        boundary line y values
    OUTPUTS
        N/A

%}

%% Setup

close all
clear
clc

global temp_F    % user chosen temperature in degrees fahrenheit
global status    % cell array containing status choices

%% Inputs

% import data from excel file and sorts it
data = xlsread('LightHeatEnergy');
tempascend = sortrows(data);

% creation of menu options
data2 = cell(1,23);
for x = 1:23
% adds the corresponding values to the menu options
    data2{x} = sprintf('(%.0f, %.0f)',tempascend(x,1),tempascend(x,2));
end

% creation of vectors to be plotted later
temps = [];
illus = [];

status = {'HEAT' 'LIGHT' 'OFF'};

flag1 = 1;
while flag1==1   % while loop 1
% Prompts user to choose values from the menu, making sure their values are
% within the given range. Prints the information about their choices to the
% command window. Will continue until user decides to stop.

    flag2 = 0;
    while flag2==0   % while loop 2
    % Promps user to choose from menu. Will be promted until they choose a
    % valid option.

        usertemp = menu('Choose the temperature in Fahrenheit and illumination in lux',data2);
        temp_F = tempascend(usertemp,1);
        illu = tempascend(usertemp,2);        
        % this is the equation to change from fahrenheit to kelvin
        temp_K = (temp_F-32)*(5/9)+273.15;

        if temp_K<260||temp_K>310
        % if the temperature is not in the the range 260-310 kelvin the
        % user is prompted to try again
            warning('The value chosen is outside the temperature limits of the program')
            fprintf('Please choose again.\n\n')
            pause(1)
        else
        % otherwise the loop ends
            flag2 = 1;
        end   % end of if statement
    end   % end of while loop 2
    
    % function shown below
    stat = McGMimStat(temp_K,illu);
    
    temps = [temps temp_K];
    illus = [illus illu];
    
    flag3 = 0;    
    while flag3==0   % while loop 3
    % asks the user if they want to try again. this loop only exists in the
    % case that the user presses x on the menu. it will continue to prompt
    % them to select from the menu until they actually select from the menu
        
        % flag becomes what the user chooses; if they choose yes, it stays
        % one and while loop 1 starts over; if it is two, while loop 1 ends
        flag1 = menu('Would you like to continue?','Yes','No');
        
        if flag1==0
        % if they didn't choose from the menu they are prompted to actually
        % choose from the menu
            warning('Choose from the menu')
            flag3 = 0;
        else
        % if they chose from the menu, this while loop ends
            flag3 = 1;
        end
    end
end

line_x = [260 280 280 280 310];
line_y = [20000 20000 0 20000 100000];
%% Plot

figure('Color','w')
plot(line_x,line_y,'k-')
McGMimPlot(temps,illus)

xlabel('Temperature (T) [K]','FontSize',12)
ylabel('Illumination (E) [lx]','FontSize',12)

%% Functions

function [STATUS] = McGMimStat(Kelvin,Lux)
%{
Lani McGuire & Collen Mims    MatLab Programming (ENGIN141)    May 6, 2019


Function Description: States the status


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        y    is the equation of the line separating heat from light 
             calculated using point slope form and basic algebra knowing 
             one point
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        Kelvin   this is the temperature in degrees kelvin which was 
                 converted from degrees Fahrenheit
        Lux      this is the illuminations chosen from the menu by the user
    OUTPUTS
        STATUS   by the textbook this variable is a string which contains 
                 either Heat Off or Light which is chosen based upon the 
                 location of the point chosen
%}

global temp_F
global status

% equation for the line is y = (8000/3)x - 726666.6667
y = (8000/3)*Kelvin - 726666.6667;
    
if Kelvin<280&&Lux<20000
% if it's in the box it's OFF
    fprintf('Temperature of %.0f degrees Fahrenheit = %.0f kelvin.\n',temp_F,Kelvin)
    fprintf('For %.0f kelvin and %.0f lux, the device is in mode %s.\n\n',Kelvin,Lux,status{3})
    STATUS = status{3};
% if it's abobe the line, it's LIGHT
elseif Lux>y
    fprintf('Temperature of %.0f degrees Fahrenheit = %.0f kelvin.\n',temp_F,Kelvin)
    fprintf('For %.0f kelvin and %.0f lux, the device is in mode %s.\n\n',Kelvin,Lux,status{2})
    STATUS = status{2};
else
% if it's below the line and next to the box, it is HEAT
    fprintf('Temperature of %.0f degrees Fahrenheit = %.0f kelvin.\n',temp_F,Kelvin)
    fprintf('For %.0f kelvin and %.0f lux, the device is in mode %s.\n\n',Kelvin,Lux,status{1})
    STATUS = status{1};
end   % end of if statement

end   % end of function

function McGMimPlot(T,I)
%{
Lani McGuire & Collen Mims    MatLab Programming (ENGIN141)    May 6, 2019


Function Description: Plots the data


VARIABLES
Top INPUTS/OUTPUTS are specific to this function. Bottom INPUTS/OUTPUTS go
in and out of the function

    INPUTS
        N/A
    OUTPUTS
        N/A
---------------------------------------------------------------------------
    INPUTS
        T    This vector contains the temperatures chosen by the user from
             the menu, already converted to degrees kelvin
        I    This vector contains the illuminations chosen by the user from 
             the menu, in sums             
    OUTPUTS
        N/A
%}

hold on
plot(T,I,'bo',...
    'MarkerSize',18,...
    'MarkerFaceColor','b')

text(262,10000,'SYSTEM OFF','FontSize',14)
text(270,70000,'LIGHT MODE','FontSize',14)
text(288,35000,'HEAT MODE','FontSize',14)

end   % end of function
