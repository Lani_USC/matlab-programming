%% Lani McGuire        MatLab Programming (ENGIN141)      February 25, 2019

% Program Description: Organize data relating to the resistor values in the
% given table/chart

%% Variables

%   INPUTS
%       Resistors - values of resistance in Ohms
%       Voltage - values of voltage in Volts
%       Data - 3x3 matrix with current correspinding to resistance and
%              voltage selected
%       Choice1 - number stored from menu that user selected resistance 
%                 from
%       Resistor_choice - resistance chosen
%       Choice2 - number stored from menu that user selected voltage from
%       Voltage_choice - voltage chosen
%       Power - 3x3 matrix containing power values corresponding to current
%               and voltage
%       L - length of Resistors array
%       NewDataSize - size of Data matrix [rows columns]
%       MaxPower - largest power value in the Power matrix
%   OUTPUTS
%       N/A

%% Setup

close all
clear
clc

%% Inputs

Resistors = {100 150 200};            % first column (resistor values [Ohms])
Voltage = {1.5 3 9};                  % first row (voltage [V])
Data = [15 30 90;10 20 60;7.5 15 45]; % data corresponding to resistors and voltage

%% User Selections

Choice1 = menu('Choose a Resistor.',Resistors); % asks user to select resistor
Resistor_choice = Resistors{Choice1};           % stores resistor chosen
Choice2 = menu('Choose a Voltage',Voltage);     % asks user to select a voltage
Voltage_choice = Voltage{Choice2};              % stores voltage chosen

% calulate power
Power = [cell2mat(Voltage);cell2mat(Voltage);cell2mat(Voltage)].*Data.*0.001;            % calculates all power values units -> [W]

%% Output

% prints the following statements to the command window, filling in
% appropriately
fprintf('\nThe chosen resistor value = %.0f [Ohms]\n',Resistor_choice)
fprintf('\nThe chosen voltage value = %.1f [V]\n',Voltage_choice)
fprintf('\nThe current in the chosen resistor due to the chosen voltage = %.3f [A]\n',Data(Choice1,Choice2))
fprintf('\nThe power corresponding to the chosen resistor and voltage = %.3f [W]\n',Power(Choice1,Choice2))

%% Additional Data

L = length(Resistors);                                                   % finds the length of the cell array Resistors
Resistors{4} = input('\n\nType the value of the resistor in [Ohms].\n'); % adds the user inputed number to the cell array Resistors
NewDataSize = [size(Data,1) size(Data,2)];                               % finds size of Data matrix

%% Extra Credit
MaxPower = max(Power,[],'all'); % finds the largest power value in the entire matrix

