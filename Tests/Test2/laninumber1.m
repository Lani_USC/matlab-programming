%{
Lani McGuire           MatLab Programming (ENGIN141)         April 4, 2019


Program Description: Test 2. 


VARIABLES

    INPUTS
        energy           given data for energy
        temp_change      given data for change in temperature
        mass             user inputed mass
        MB               find m and b value for equaiton
        m                m value for the theoretical data equation
        b                b value for the theoretical data equation
        x                x-values for the theoretical data
        y                y-values for the theoretical data
        equation         equation of the line of best fit
        lgd              legend handle
        specific_heat    specific heat calculated from user mass and the
                         slope of the line of best fit
    OUTPUTS

%}

%% Setup

close all
clear
clc

%% Inputs

energy = [17 40 58]; % units -> [J]
temp_change = [2 5 7]; % units -> [K]
mass = input('Input the mass in kilograms:\n');

%% Plot

figure('color','w')

plot(energy,temp_change,'bd',...
    'MarkerSize',12,...
    'MarkerEdgeColor','b',...
    'MarkerFaceColor','b');
        
title('Energy Applied vs. Temperature Change','FontSize',16)

grid on
grid minor

axis([10 60 0 10])

xlabel('Heat Energy applied (Q) [J]',...
       'FontSize',14,...
       'FontWeight','bold')
ylabel('Temperature Change (\Delta T) [K]',...
       'FontSize',14,...
       'FontWeight','bold')

%% Trend line

MB = polyfit(energy,temp_change,1);
m = MB(1);
b = MB(2);
x = min(energy):max(temp_change);
y = m*x+b;
%{
equation = sprintf('\Delta T = %.3f Q + %.3f',m,b);
matlab literally won't accept (\Delta) even though it works on the axis
label
%}  
equation = sprintf('y = %.3f x + %.3f',m,b);
hold on 

plot(energy,temp_change,'b.-',...
    'MarkerSize',1,...
    'MarkerEdgeColor','b',...
    'MarkerFaceColor','b');

%% Features of Plot

lgd = legend('experimental data',equation);
lgd.Location = 'best';
lgd.FontSize = 12;

%% Calculate Specific Heat

specific_heat = (m^(-1))/mass;

%% Outputs

fprintf('The specific heat of the material of mass %.3f is %.3f.\n\n',mass,specific_heat)
