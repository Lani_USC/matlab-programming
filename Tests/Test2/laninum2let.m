%{
Lani McGuire           MatLab Programming (ENGIN141)         April 4, 2019


Function Description: Will take a number grade and turn it into a letter
grade.


VARIABLES

    INPUTS
        number    a quiz grade; preferably in the range 0-10
    OUTPUTS
        letter    the letter grade of the inputed number grade
%}

function[letter] = laninum2let(number)

    % if the number is 9-10 (inclusive)
    if number>=9&&number<=10
        letter = 'A';
    
    % if the number is 8-9 (not including 9)
    elseif number>=8&&number<9
        letter = 'B';
        
    % if the number is 7-8 (not including 8)
    elseif number>=7&&number<8
        letter = 'C';
        
    % if the number is 6-7 (noy including 7)
    elseif number>=6&&number<7
        letter = 'D';
        
    % if the number is below 6
    elseif number>=0&&number<6
        letter = 'F';
    
    % if the number is not inside the range 1-10
    else
        letter = 'X';
        
    end % end of if statement

end % end of function
